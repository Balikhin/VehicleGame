// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "VehicleGame.h"

#include "VehiclePlayerCameraManager.generated.h"

UCLASS()
class VEHICLEGAME_API AVehiclePlayerCameraManager : public APlayerCameraManager
{
	GENERATED_UCLASS_BODY()

};
